
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post">
        @csrf
        <p>First Name:</p>
        <input type="text" name="nama_depan" id="nama_depan">        
        <p>Last Name:</p>
        <input type="text" name="nama_belakang" id="nama_belakang">

        <p>Gender:</p>
        <input type="radio" name="Gender" value="0">Male <br>
        <input type="radio" name="Gender" value="1">Female <br>
        <input type="radio" name="Gender" value="2">Other

        <p>Nationality:</p>
        <select name="Nationality">
            <option value="0">Indonesia</option>
            <option value="1">Amerika</option>
            <option value="2">Inggris</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="Bahasa" value="0">Indonesia <br>
        <input type="checkbox" name="Bahasa" value="1">Inggris <br>
        <input type="checkbox" name="Bahasa" value="2">Other
        
        <p>Bio:</p>
        <textarea name="teksarea" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>